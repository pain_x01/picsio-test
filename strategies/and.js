module.exports = function strategy(possibleDestinations) {
    const destinations = {};

    possibleDestinations.forEach(element => {
        Object.keys(element).forEach(destinationName => {
            if (typeof destinations[destinationName] === 'undefined') {
                destinations[destinationName] = element[destinationName];
            } else {
                if (element[destinationName] === false) {
                    destinations[destinationName] = false;
                }
            }
        });
    });

    return Object.keys(destinations).filter(element => destinations[element]);
}
module.exports = function strategy(possibleDestinations) {
    const destinations = [];

    possibleDestinations.forEach(element => {
        Object.keys(element).forEach(destinationName => {
            if(element[destinationName] && !destinations.includes(destinationName)) {
                destinations.push(destinationName);
            }
        });
    });

    return destinations;
}
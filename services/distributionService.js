const request = require('request-promise');

let strategy;

switch (process.env.STRATEGY) {
    case 'OR':
        strategy = require('../strategies/or');
        break;
    case 'AND':
        strategy = require('../strategies/and');
        break;

    default:
        strategy = null;
        break;
}

async function sendRequest(array, payload) {
    const data = {};
    for (const item of array) {
        try {
            data[item] = await request({
                method: global.recipients[item].method,
                uri: global.recipients[item].url,
                body: payload,
                json: true
            });
        } catch (error) {
            data[item] = error.message;
        }
    }
    return data;
}

module.exports = function distributionService(payload, possibleDestinations) {
    return new Promise(async (resolve, reject) => {
        if (strategy) {
            let filteredDestinationNames = strategy(possibleDestinations);
            const unknownDestinations = {};

            filteredDestinationNames = filteredDestinationNames.filter(element => {
                if (global.recipients[element]) {
                    return true;
                } else {
                    unknownDestinations[element] = `url unknown for ${element}`;
                    return false;
                }
            });

            const requests = await sendRequest(filteredDestinationNames, payload);
            const response = Object.assign(requests, unknownDestinations);

            resolve(response);

        } else {
            reject(new Error('No strategy'));
        }
    });
}
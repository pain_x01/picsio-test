#!/bin/bash

docker run -itd --name picsio-test -v $(pwd)/data:/usr/src/app/data -p 3001:3001 -e PORT=3001 -e STRATEGY=OR test node ./app.js

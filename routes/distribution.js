const express = require('express');
const router = express.Router();
const distributionService = require('../services/distributionService');

router.post('/', async (req, res, next) => {
  const body = req.body;

  if (body.payload && body.possibleDestinations && body.possibleDestinations.length) {
    const response = await distributionService(body.payload, body.possibleDestinations);
    res.send(response);
  } else {
    const error = new Error('payload or possibleDestinations is missing');
    error.status = 400;
    return next(error);
  }
});

module.exports = router;

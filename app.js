const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const errorHandler = require('errorhandler');
const fs = require('fs');
const path = require('path');
const distributionRouter = require('./routes/distribution');


try {
    global.recipients = {};
    const recipients = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/recipients.json'), { encoding: 'utf-8' }));
    if (recipients && recipients.length) {
        recipients.forEach(element => {
            global.recipients[element.name] = {
                method: element.type.substring(5).toUpperCase(),
                url: element.url
            };
        });
    } else {
        console.error('Recipients list is empty');
        process.exit(1);
    }
} catch (error) {
    console.error(error);
    process.exit(1);
}

const app = express();
const port = process.env.PORT || 3001;

app.use(errorHandler());
app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use((req, res, next) => {
    // Fake authorization check
    if (req.headers.authorization) {
        next();
    } else {
        const error = new Error('Unauthorized')
        error.status = 401;
        return next(error);
    }
})

app.use('/distribution', distributionRouter);

app.listen(port, () => {
    console.log(`Server is listening on port ${port}!`);
});


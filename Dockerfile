FROM node:12.5.0-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY routes ./routes
COPY services ./services
COPY strategies ./strategies
COPY app.js ./app.js
COPY package.json ./package.json


RUN npm set progress=false && \
    npm install -s --no-progress

EXPOSE 3001
